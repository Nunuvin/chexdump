CC = gcc
FLAGS = -Wall -pedantic -Wextra -Werror

all:
	${CC} hex.c -o hex ${FLAGS}