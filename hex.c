#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

#define BLOCK 4096
#define LINESIZE 8



int print_line(int bytes[], long addr){
    long i=0;
    printf("%08lX:  ",addr-LINESIZE+1);
    for(i=0;i<LINESIZE;i++){
        printf("%02X",bytes[i]);
        if (((i-1)%2==0) && (i != 0)){
            printf(" ");
        }
    }
    printf("|");
    for(i=0;i<LINESIZE;i++){
        printf("%c",(char)bytes[i]);
        if (((i-1)%2==0) && (i != 0)){
            printf(" ");
        }
    }
    printf("\n");
    return 0;
}


int main(int argc, char** argv){
    if(argc < 2){
        printf("Usage: %s file [-n bytes]\n",argv[0]);
        return 0;
    }
    FILE *file;
    file = fopen(argv[1], "rb");

    if(access(argv[1], F_OK) != 0){
        printf("File does not exist\n");
        return 0;
    }

    int bs[LINESIZE];
    
    if ((argc == 4) && (!strncmp(argv[2], "-n",3))) {
        //print n bytes
        long sz = atoi(argv[3]);
        long i = 0;
        while (i < sz){
            bs[i % LINESIZE] = fgetc(file);
            if(feof(file)){
                break;
            }
            if((i % LINESIZE == (LINESIZE - 1)) && (i != 0)){
                print_line(bs, i);
            }
            i++;
        }
    }else if(argc == 2){
        //print entire file
        long i = 0;
        while (1){
            bs[i % LINESIZE] = fgetc(file);
            if(feof(file)){
                break;
            }
            if((i % LINESIZE == (LINESIZE - 1)) && (i != 0)){
                print_line(bs, i);
                for(long j=0;j<LINESIZE;j++){
                    bs[j] = ' ';
                }
            }
            i++;
        }
    }else{
      printf("Usage: %s file [-n bytes]\n",argv[0]);  
    }


    fclose(file);
    return 0;
}

